"""CloudEventful unit tests."""
import json
import re
from datetime import datetime

import pytest
from pydantic import BaseModel

from cloudeventful import CloudEvent, CloudEventful

ce = CloudEventful(api_version="1.0.0", default_source="ECTests")


@ce.data_model(re.compile(r"/.*/coffee"), subject_factory=lambda m: m.flavor)
class Coffee(BaseModel):
    """A daily necessity."""

    flavor: str
    cream: int = 0


@ce.data_model(re.compile(r"/.*/utensils"))
class Pen(BaseModel):
    """Write with it."""

    color: str
    refillable: bool


def test_cloud_event() -> None:
    event = ce.event(Coffee(flavor="mocha"))
    assert event.data.flavor == "mocha"
    assert event.subject == "mocha"


def test_discover() -> None:
    assert ce.discover().data_models == {
        r"/.*/coffee": Coffee.schema(),
        r"/.*/utensils": Pen.schema(),
    }


def test_ce_properties() -> None:
    ce.api_version = "2"
    ce.default_source = "DefaultSource"
    ce.default_id_factory = lambda m: type(m).__name__
    ce.default_specversion = "3"
    ce.default_type_factory = lambda _m: "DefaultType"
    ce.default_datacontenttype = "application/yaml"
    ce.default_dataschema_factory = lambda _m: "DefaultDataSchema"
    ce.default_subject_factory = lambda _m: "DefaultSubject"
    now = datetime.now()
    ce.default_time_factory = lambda: now
    event = ce.event(Pen(color="black", refillable=True))
    assert event.source == "DefaultSource"
    assert event.id == "Pen"
    assert event.specversion == "3"
    assert event.type == "DefaultType"
    assert event.datacontenttype == "application/yaml"
    assert event.dataschema == "DefaultDataSchema"
    assert event.subject == "DefaultSubject"
    assert event.time == now
    assert ce.api_version == "2"


def test_publish() -> None:
    model = Coffee(flavor="mocha")

    def _publish(_topic: str, event: str) -> None:
        event = CloudEvent(**json.loads(event))
        assert isinstance(event, CloudEvent)
        assert event.data == model

    with pytest.raises(RuntimeError):
        ce.publish(model, "/mocha/coffee")

    ce.publish_function = _publish
    with pytest.raises(ValueError):
        ce.publish(model, "/mocha/tea")
    ce.publish(model, "/mocha/coffee")


def test_topic_factory() -> None:
    ce.default_topic_factory = lambda m: f"/api/v1/{type(m).__name__.lower()}"

    def _publish(topic: str, _event: str) -> None:
        assert topic == "/api/v1/coffee"

    model = Coffee(flavor="mocha")
    ce.publish_function = _publish
    ce.publish(model)


def test_no_topic_error() -> None:
    ce.default_topic_factory = None
    ce.publish_function = lambda _data, _topic: None
    model = Pen(color="black", refillable=True)
    with pytest.raises(RuntimeError):
        ce.publish(model)


def test_duplicate_pattern() -> None:
    with pytest.raises(ValueError):
        ce.data_model(re.compile(r"/.*/coffee"))(Pen)
